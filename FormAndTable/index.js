import React, { useState, useEffect, useCallback } from "react";

import { Box } from "@material-ui/core";
import { PageHeader, Button, Finder, ButtonBox } from "mio-library-ui";

import ConfirmDeleteDialog from "~/components/ConfirmDeleteDialog";

import Table from "./components/Table";
import Form from "./components/Form";

import useDialogNotification from "~/hooks/useDialogNotification";
import useNotification from "~/hooks/useNotification";
import useDialog from "~/hooks/useDialog";

import api from "~/services/api-pessoal";

const Exemplo = () => {
  const [collection, setCollection] = useState({
    isLoading: false,
    itens: [],
  });
  const [query, setQuery] = useState("");

  const notification = useNotification();
  const dialogNotification = useDialogNotification();
  const {
    data: dataForm,
    isOpen: isOpenForm,
    close: closeForm,
    open: openForm,
  } = useDialog();
  const {
    data: idDelete,
    isOpen: isOpenDelete,
    isLoading: isLoadingDelete,
    changeLoading: changeLoadingDelete,
    close: closeDelete,
    open: openDelete,
  } = useDialog();

  useEffect(() => {
    const getCollection = async () => {
      setCollection((oldState) => ({
        ...oldState,
        isLoading: true,
      }));

      try {
        const response = await api.get("/Exemplo");
        if (response.data.data) {
          setCollection((oldState) => ({
            ...oldState,
            itens: response.data.data,
          }));
        }
      } catch (err) {
        dialogNotification.extractErrors(err);
      }
      setCollection((oldState) => ({
        ...oldState,
        isLoading: false,
      }));
    };
    getCollection();
    // eslint-disable-next-line
  }, []);

  const handleQuery = useCallback((q) => {
    setQuery(q);
  }, []);

  const handleClickItem = useCallback(
    (event, value) => {
      const handleClickEditItem = (id) => {
        const { itens } = collection;
        const item = itens.find((i) => i.id === id);
        openForm(item);
      };

      const handleClickDeleteItem = (id) => openDelete(id);

      const functions = {
        edit: handleClickEditItem,
        delete: handleClickDeleteItem,
      };
      functions[event](value);
    },
    [collection, openForm, openDelete]
  );

  const handleDeleteItem = useCallback(async () => {
    changeLoadingDelete();
    const itens = collection.itens;
    try {
      await api.delete(`/Exemplo/${idDelete}`);
      const newItens = itens.filter((i) => i.id !== idDelete);
      closeDelete();
      setCollection((oldState) => ({
        ...oldState,
        itens: newItens,
      }));
      notification.remove();
    } catch (err) {
      dialogNotification.extractErrors(err);
    }
    //eslint-disable-next-line
  }, [collection.itens, changeLoadingDelete, closeDelete, idDelete]);

  const handleClickAddItem = useCallback(() => {
    openForm();
  }, [openForm]);

  const handleAfterSubmitForm = useCallback(
    (event, value) => {
      const handleAfterInsert = (data) => {
        const { itens } = collection;
        const newItens = [data, ...itens];
        setCollection((oldState) => ({
          ...oldState,
          itens: newItens,
        }));
        closeForm();
      };

      const handleAfterUpdate = (data) => {
        const { itens } = collection;
        const newItens = itens.map((i) => (i.id === data.id ? data : i));
        setCollection((oldState) => ({
          ...oldState,
          itens: newItens,
        }));
        closeForm();
      };

      const functions = {
        insert: handleAfterInsert,
        update: handleAfterUpdate,
      };

      functions[event](value);
    },
    [collection, closeForm]
  );

  return (
    <Box height="100%" width="100%" p={2}>
      <PageHeader title="Exemplo">
        <ButtonBox>
          <Button
            size="small"
            color="primary"
            variant="contained"
            onClick={handleClickAddItem}
          >
            Adicionar
          </Button>
          <Finder onSearch={handleQuery} onClose={() => handleQuery("")} />
        </ButtonBox>
      </PageHeader>

      <Table
        data={collection.itens}
        isLoading={collection.isLoading}
        query={query}
        onItemClick={handleClickItem}
      />

      <Form
        isOpen={isOpenForm}
        data={dataForm}
        onClose={closeForm}
        onAfterSubmitForm={handleAfterSubmitForm}
      />

      <ConfirmDeleteDialog
        isOpen={isOpenDelete}
        isDeleting={isLoadingDelete}
        onCancel={closeDelete}
        onConfirm={handleDeleteItem}
      />
    </Box>
  );
};

export default Exemplo;
