import React, { useMemo } from "react";

import { IconButton } from "@material-ui/core";
import { DataTable, ButtonBox } from "mio-library-ui";

import { Edit as EditIcon, Delete as DeleteIcon } from "@material-ui/icons";

const Table = (props) => {
  const { data, onItemClick, isLoading, query } = props;

  const columns = useMemo(
    () => [
      {
        name: "nome",
        label: "Nome",
      },
      {
        name: "id",
        label: "Ações",
        options: {
          filter: true,
          sort: false,
          empty: true,
          customBodyRender: (value) => {
            return (
              <ButtonBox spacing={0} justifyContent="center">
                <IconButton
                  size="small"
                  title="Editar este registro"
                  color="primary"
                  aria-label="Editar"
                  onClick={() => {
                    onItemClick("edit", value);
                  }}
                >
                  <EditIcon fontSize="small" color="primary" />
                </IconButton>

                <IconButton
                  title="Deletar este registro"
                  size="small"
                  color="primary"
                  aria-label="Deletar"
                  onClick={() => {
                    onItemClick("delete", value);
                  }}
                >
                  <DeleteIcon fontSize="small" color="primary" />
                </IconButton>
              </ButtonBox>
            );
          },
        },
      },
    ],
    [onItemClick]
  );

  return (
    <DataTable
      title=""
      data={data}
      columns={columns}
      isLoading={isLoading}
      sherlock={{
        query,
        columns: ["nome"],
      }}
    />
  );
};

Table.defaultProps = {
  query: "",
};

export default Table;
