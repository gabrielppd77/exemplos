import React, { useState, useEffect, useCallback } from "react";

import { Grid } from "@material-ui/core";
import { ActionDialog, TextField } from "mio-library-ui";

import useDialogNotification from "~/hooks/useDialogNotification";
import useNotification from "~/hooks/useNotification";

import api from "~/services/api-pessoal";

import * as yup from "yup";

const schema = yup.object().shape({
  nome: yup.string().required("Informe o Nome"),
});

const Form = (props) => {
  const { isOpen, onClose, data: _data, onAfterSubmitForm } = props;
  const [data, setData] = useState({});
  const [validationErrors, setValidationErrors] = useState(false);
  const [isSubmitting, setSubmitting] = useState(false);

  const notification = useNotification();
  const dialogNotification = useDialogNotification();

  useEffect(() => {
    setData(_data);
    setValidationErrors(false);
  }, [_data]);

  const handleSubmit = useCallback(() => {
    const update = async () => {
      setSubmitting(true);
      try {
        const response = await api.put(`/Exemplo/${data.id}`, data);
        onAfterSubmitForm("update", response.data.data);
        notification.put();
      } catch (err) {
        dialogNotification.extractErrors(err);
      }
      setSubmitting(false);
    };

    const insert = async () => {
      setSubmitting(true);
      try {
        const response = await api.post("/Exemplo", data);
        notification.post();
        onAfterSubmitForm("insert", response.data.data);
      } catch (err) {
        dialogNotification.extractErrors(err);
      }
      setSubmitting(false);
    };

    setValidationErrors(false);
    schema
      .validate(data, { abortEarly: false })
      .then(() => {
        if (data.id) {
          update();
          return;
        }
        insert();
      })
      .catch((err) => {
        setValidationErrors(err);
      });
    //eslint-disable-next-line
  }, [data, onAfterSubmitForm]);

  return (
    <ActionDialog
      title="Cadastro de Exemplo"
      isOpen={isOpen}
      onClose={onClose}
      okLabel="Salvar"
      isOkProcessing={isSubmitting}
      onOkClick={handleSubmit}
      onCancelClick={onClose}
      dialogProps={{ maxWidth: "lg", fullWidth: true }}
    >
      <Grid container spacing={2}>
        <Grid xl={12} lg={12} md={12} sm={12} xs={12} item>
          <TextField
            label="Nome"
            name="nome"
            validationErrors={validationErrors}
            required
            variant="outlined"
            size="small"
            fullWidth
            value={data?.nome || ""}
            onChange={(e) => {
              const nome = e.target.value;
              setData((oldState) => ({
                ...oldState,
                nome,
              }));
            }}
          />
        </Grid>
      </Grid>
    </ActionDialog>
  );
};

export default Form;
